﻿using CeeLearnAndDo.App_Data;
using CeeLearnAndDo.App_Data.CeeLearnAndDoDataSetTableAdapters;
using CeeLearnAndDo.App_Start;
using CeeLearnAndDo.Models;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI.WebControls;
using System.Xml;
using TAlex.RssFeedGenerator;
using TAlex.RssFeedGenerator.Models;

namespace CeeLearnAndDo.Controllers {
    public class HomeController : Controller {

        public static bool LoggedIn { get; set; }
        public static DatabaseModels.User CurrentUser;

        public static List<DatabaseModels.Article> ArticleList = DatabaseQueries.RefreshArticles();
        public static List<DatabaseModels.Reference> ReferenceList = DatabaseQueries.RefreshReferences();
        public static DatabaseModels.Article PickedArticle = ArticleList[0];
        public static DatabaseModels.Reference PickedReference = ReferenceList[0];
        public static string RssText { get; set; }

        public ActionResult Index() {
            ViewBag.Title = "Welkom op CeeLearnAndDo";

            return View();
        }

        public ActionResult Articles() {
            ViewBag.Title = "Artikelen";
            if (DatabaseQueries.GetArticles()) {
                return View("Articles");
            }
            return View("Thanks");
        }

        public ActionResult Contact() {
            ViewBag.Title = "Contact";
            ViewBag.Message = "Indien u suggesties of vragen heeft voor CeeLearnAndDo, kunt u hier een bericht sturen.";

            return View();
        }

        public ActionResult References() {
            DatabaseQueries.RefreshReferences();
            ViewBag.Title = "Referenties";
            ViewBag.Message = "Referenties van bedrijven die zijn geholpen door CeeLearnAndDo.";

            return View();
        }

        public ActionResult Signin() {
            ViewBag.Title = "Inloggen";
            ViewBag.Message = "Log in";

            return View();
        }

        public ActionResult Admin() {
            if (LoggedIn) {
                ViewBag.Title = "Admin";
                ViewBag.Message = "Dit is de admin pagina";

                return View();
            }
            ViewBag.Title = "Toegang geweigerd!";
            ViewBag.Message = "U moet ingelogd zijn om deze pagina te zien";
            return View("Thanks");
        }

        public ActionResult ViewArticle(int ArticleId) {
            ViewBag.Title = "Artikelen";
            DatabaseQueries.GetArticle(ArticleId);
            return View();
        }

        public ActionResult ViewReference(int ReferenceId) {
            ViewBag.Title = "Referenties";
            DatabaseQueries.GetReference(ReferenceId);
            return View();
        }

        public ActionResult TellAFriend(FormCollection form) {
            string name = form["Name"];
            string email = form["Email"];
            string message = form["Message"];

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Port = 25;
            smtpClient.Host = "127.0.0.1";
            smtpClient.Send(email, "bryan.zantvoort@gmail.com", String.Format("Bericht van {0}", name), message);

            return View("ViewArticle");
        }

        public ActionResult AdminViewArticle(int ArticleId) {
            DatabaseQueries.GetArticle(ArticleId);

            ViewBag.Title = "Admin";
            return View("Admin");
        }

        public ActionResult EditArticle(int ArticleId) {

            if ((PickedArticle = DatabaseQueries.GetArticle(ArticleId)) == null) {
                ViewBag.Title = "Er ging iets mis";
                ViewBag.Message = String.Format(
                    "Er is iets mis gegaan bij de verbinding naar de database.");

                return View("Thanks");
            }
            HtmlClass art = new HtmlClass();
            art.HtmlContent = PickedArticle.Content;
            ViewBag.Message = null;
            ViewBag.Title = "Artikel Bewerken";
            return View(art);
        }
        public ActionResult Feed() {
            DatabaseQueries.GetArticles();

            Rss rss = new Rss();

            rss.Channel.Title = "CeeLearnAndDo RSS Feed";
            foreach (DatabaseModels.Article article in HomeController.ArticleList) {
                Item item = new Item();
                item.Title = article.Title;
                item.Link = String.Format("http://localhost:21812/Home/ViewArticle?ArticleId={0}", article.Id);
                if (article.Content.Length >= 150) {
                    article.Content = article.Content.Substring(0, 150);
                }
                item.Description = article.Content;
                item.Author = article.Author;

                rss.Channel.Items.Add(item);
            }

            MemoryStream output = new MemoryStream();

            FeedGenerator generator = new FeedGenerator();
            generator.Generate(rss, output);

            System.IO.File.WriteAllText(Server.MapPath("~/Content/rssfeed.xml"), String.Empty);
            FileStream fileStream = System.IO.File.OpenWrite(Server.MapPath("~/Content/rssfeed.xml"));
            fileStream.Write(output.ToArray(), 0, Convert.ToInt32(output.Length));
            fileStream.Close();

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(Server.MapPath("~/Content/rssfeed.xml"));
            RssText = xmlDocument.InnerXml;

            return View();
        }

        [ValidateInput(false), HttpPost]
        public ActionResult Edit(FormCollection form, HtmlClass model, HttpPostedFileBase file) {

            string fileName = String.Empty;
            string path = String.Empty;
            if (file != null && file.ContentLength > 0) {
                fileName = Path.GetFileName(file.FileName);
                path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                file.SaveAs(path);

                path = Path.Combine("~/Images/", fileName);
            }
            else {
                path = PickedArticle.ImagePath;
            }
            DatabaseQueries.EditArticle(form["Title"], model.HtmlContent, path);
            return View("Admin");
        }

        [ValidateInput(false), HttpPost]
        public ActionResult EditReference(FormCollection form, HtmlClass model, HttpPostedFileBase file) {

            string fileName = String.Empty;
            string path = String.Empty;
            if (file != null && file.ContentLength > 0) {
                fileName = Path.GetFileName(file.FileName);
                path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                file.SaveAs(path);

                path = Path.Combine("~/Images/", fileName);
            }
            else {
                path = PickedArticle.ImagePath;
            }
            DatabaseQueries.AddReference(form["Title"], model.HtmlContent, form["Link"], path);
            return View("Admin");
        }

        public ActionResult Delete(int ArticleId) {
            DatabaseQueries.DeleteArticle(ArticleId);

            ViewBag.Title = "Admin";
            return View("Admin");
        }


        public ActionResult NewArticle(HtmlClass model) {
            ViewBag.Title = "Nieuw Artikel";
            return View();

        }
        public ActionResult NewReference(HtmlClass model) {
            ViewBag.Title = "Nieuwe Referentie";
            return View();
        }

        [ValidateInput(false), HttpPost]
        public ActionResult Add(FormCollection form, HtmlClass model, HttpPostedFileBase file) {

            string fileName = String.Empty;
            string path = String.Empty;
            if (file != null && file.ContentLength > 0) {
                fileName = Path.GetFileName(file.FileName);
                path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                file.SaveAs(path);
            }

            path = Path.Combine("~/Images/", fileName);
            DatabaseQueries.AddArticle(form["Title"], model.HtmlContent, CurrentUser.Username, path);

            ViewBag.Title = "Admin";
            return View("Admin");
        }

        [HttpPost]
        public ActionResult SignInForm(FormCollection form) {
            if (DatabaseQueries.SignIn(form["Username"], form["Password"])) {
                return View("Admin");
            }

            return View("Thanks");
        }

        public ActionResult Logout() {
            LoggedIn = false;

            ViewBag.Title = "U bent nu uitgelogd.";
            return View("Thanks");
        }

        [HttpPost]
        public ActionResult ContactForm(FormCollection form) {
            DatabaseQueries.AddMessage(form["Name"], form["Email"], form["Message"]);

            return View("Thanks");
        }

    }
}
