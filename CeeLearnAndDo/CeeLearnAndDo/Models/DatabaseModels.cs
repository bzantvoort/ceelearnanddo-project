﻿using System.Runtime.InteropServices;

namespace CeeLearnAndDo.Models {
    public class DatabaseModels {
        public class Article {
            public int Id { get; set; }
            public string Title { get; set; }
            public string Content { get; set; }
            public string Author { get; set; }
            public string ImagePath { get; set; }
        }

        public class Reference
        {
            public int Id { get; set; }
            public string Title { get; set; }
            public string Content { get; set; }
            public string Link { get; set; }
            public string ImagePath { get; set; }
        }
        public class Message
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string Text { get; set; }
        }

        public class User
        {
            public int Id { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
        }
    }
}