﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CeeLearnAndDo.Models {
    public class HtmlClass {

        [AllowHtml]
        public string HtmlContent { get; set; }

        public HtmlClass() {

        }
    }
}