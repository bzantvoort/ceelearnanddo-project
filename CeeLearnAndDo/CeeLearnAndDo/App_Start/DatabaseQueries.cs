﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using CeeLearnAndDo.Controllers;
using CeeLearnAndDo.Models;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Web.Configuration;
using Microsoft.Ajax.Utilities;
using TAlex.RssFeedGenerator;
using TAlex.RssFeedGenerator.Models;

namespace CeeLearnAndDo.App_Start {
    public class DatabaseQueries : Controller {

        public static List<DatabaseModels.Article> RefreshArticles() {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString)) {
                using (SqlCommand selectCommand = new SqlCommand()) {
                    selectCommand.Connection = conn;
                    selectCommand.CommandText = "Select * from [Articles]";

                    try {
                        conn.Open();
                        selectCommand.ExecuteNonQuery();
                    }
                    catch (SqlException) {

                    }
                    SqlDataReader reader = selectCommand.ExecuteReader();

                    var articles = new List<DatabaseModels.Article>();
                    while (reader.Read()) {
                        articles.Add(new DatabaseModels.Article {
                            Author = reader["ArticleAuthor"].ToString(),
                            Content = reader["ArticleContent"].ToString(),
                            Id = Convert.ToInt32(reader["ArticleId"]),
                            Title = reader["ArticleTitle"].ToString(),
                            ImagePath = reader["ArticleImagePath"].ToString()
                        });
                    }
                    articles.Reverse();
                    return articles;
                }
            }
        }
        public static bool GetArticles() {
            using (
                SqlConnection conn =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString)) {
                using (SqlCommand selectCommand = new SqlCommand()) {
                    selectCommand.Connection = conn;
                    selectCommand.CommandText = "Select * from [Articles]";

                    try {
                        conn.Open();
                        selectCommand.ExecuteNonQuery();
                    }
                    catch (SqlException) {
                        return false;
                    }
                    SqlDataReader reader = selectCommand.ExecuteReader();

                    var articles = new List<DatabaseModels.Article>();
                    while (reader.Read()) {
                        articles.Add(new DatabaseModels.Article {
                            Author = reader["ArticleAuthor"].ToString(),
                            Content = reader["ArticleContent"].ToString(),
                            Id = Convert.ToInt32(reader["ArticleId"]),
                            Title = reader["ArticleTitle"].ToString(),
                            ImagePath = reader["ArticleImagePath"].ToString()
                        });
                    }
                    articles.Reverse();
                    HomeController.ArticleList = articles;
                }
            }

            return true;
        }
        public static DatabaseModels.Article GetArticle(int ArticleId) {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString)) {
                using (SqlCommand selectCommand = new SqlCommand()) {
                    selectCommand.Connection = conn;
                    selectCommand.CommandText = "Select * from [Articles] where ArticleId = @Id";
                    selectCommand.Parameters.Add("@Id", SqlDbType.Int, 50).Value = ArticleId;
                    try {
                        conn.Open();
                        selectCommand.ExecuteNonQuery();
                    }
                    catch (SqlException) {
                        return null;
                    }
                    SqlDataReader reader = selectCommand.ExecuteReader();

                    var article = new DatabaseModels.Article();
                    while (reader.Read()) {
                        article.Author = reader["ArticleAuthor"].ToString();
                        article.Content = reader["ArticleContent"].ToString();
                        article.Id = Convert.ToInt32(reader["ArticleId"]);
                        article.Title = reader["ArticleTitle"].ToString();
                        article.ImagePath = reader["ArticleImagePath"].ToString();
                    }
                    HomeController.PickedArticle = article;
                }
            }
            return HomeController.PickedArticle;
        }
        public static void DeleteArticle(int ArticleId) {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString)) {
                using (SqlCommand selectCommand = new SqlCommand()) {
                    selectCommand.Connection = conn;
                    selectCommand.CommandText = "Delete from [Articles] where ArticleId = @Id";
                    selectCommand.Parameters.Add("@Id", SqlDbType.Int, 50).Value = ArticleId;
                    try {
                        conn.Open();
                        selectCommand.ExecuteNonQuery();
                    }
                    catch (SqlException) {
                    }
                    HomeController.ArticleList = RefreshArticles();

                    HomeController.PickedArticle.Title = "Verwijderd";
                    HomeController.PickedArticle.Content = "Dit artikel is verwijderd.";
                    HomeController.PickedArticle.Author = "-";
                }
            }
        }
        public static void AddArticle(string title, string content, string author, string imagePath) {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString)) {
                string queryString = "Insert into Articles (ArticleTitle, ArticleContent, ArticleAuthor, ArticleImagePath) Values (@articleTitle, @articleContent, @articleAuthor, articleImagePath)";

                using (SqlCommand insertCommand = new SqlCommand()) {
                    insertCommand.Connection = conn;
                    insertCommand.CommandText = queryString;
                    insertCommand.Parameters.Add("@articleTitle", SqlDbType.NVarChar, 50).Value = title;
                    insertCommand.Parameters.Add("@articleContent", SqlDbType.NText, int.MaxValue).Value = content;
                    insertCommand.Parameters.Add("@articleAuthor", SqlDbType.NChar, 25).Value = author;
                    insertCommand.Parameters.Add("@articleImagePath", SqlDbType.NVarChar, 150).Value = imagePath;

                    try {
                        conn.Open();
                        insertCommand.ExecuteNonQuery();
                    }
                    catch (SqlException) {
                    }

                    HomeController.ArticleList = RefreshArticles();
                }
            }
        }
        public static void AddReference(string title, string content, string link, string imagePath) {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString)) {
                string queryString = "Insert into [References] (ReferenceTitle, ReferenceContent, ReferenceLink, ReferenceImagePath) Values (@referenceTitle, @referenceContent, @referenceLink, @referenceImagePath)";

                using (SqlCommand insertCommand = new SqlCommand()) {
                    insertCommand.Connection = conn;
                    insertCommand.CommandText = queryString;
                    insertCommand.Parameters.Add("@referenceTitle", SqlDbType.NVarChar, 50).Value = title;
                    insertCommand.Parameters.Add("@referenceContent", SqlDbType.NText, int.MaxValue).Value = content;
                    insertCommand.Parameters.Add("@referenceLink", SqlDbType.NVarChar, 150).Value = link;
                    insertCommand.Parameters.Add("@referenceImagePath", SqlDbType.NVarChar, 150).Value = imagePath;

                    conn.Open();
                    insertCommand.ExecuteNonQuery();

                    HomeController.ReferenceList = RefreshReferences();
                }
            }
        }
        public static bool SignIn(string username, string password) {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString)) {
                using (SqlCommand selectCommand = new SqlCommand()) {
                    selectCommand.Connection = conn;
                    selectCommand.CommandText = "Select * from [Users] where Username = @Username";
                    selectCommand.Parameters.Add("@Username", SqlDbType.NVarChar, 50).Value = username;

                    try {
                        conn.Open();
                        selectCommand.ExecuteNonQuery();
                    }
                    catch (SqlException) {
                        return false;
                    }
                    SqlDataReader reader = selectCommand.ExecuteReader();

                    DatabaseModels.User user = new DatabaseModels.User();
                    while (reader.Read()) {
                        user.Id = Convert.ToInt32(reader["UserId"]);
                        user.Username = reader["Username"].ToString();
                        user.Password = reader["Password"].ToString();
                    }
                    if (user.Password == password && user.Username == username) {
                        HomeController.CurrentUser = user;
                        HomeController.LoggedIn = true;
                    }
                    return true;
                }
            }
        }
        public static void AddMessage(string name, string email, string message) {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString)) {
                string queryString = "Insert into Messages (Name, Email, Message) Values (@name, @email, @message)";

                using (SqlCommand insertCommand = new SqlCommand()) {
                    insertCommand.Connection = conn;
                    insertCommand.CommandText = queryString;
                    insertCommand.Parameters.Add("@name", SqlDbType.NVarChar, 50).Value = name;
                    insertCommand.Parameters.Add("@Email", SqlDbType.NVarChar, 50).Value = email;
                    insertCommand.Parameters.Add("@Message", SqlDbType.NVarChar, 500).Value = message;

                    try {
                        conn.Open();
                        insertCommand.ExecuteNonQuery();
                    }
                    catch (SqlException) {
                    }

                    SmtpClient smtpClient = new SmtpClient();
                    smtpClient.Port = 25;
                    smtpClient.Host = "127.0.0.1";
                    smtpClient.Send(email, "bryan.zantvoort@gmail.com", String.Format("Bericht van {0}", name), message);
                }
            }
        }
        public static void EditArticle(string title, string content, string imagePath) {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString)) {

                string queryString = "Update Articles set ArticleTitle = @title, articleContent = @content, ArticleImagePath = @imagePath where ArticleId = @articleId";

                using (SqlCommand insertCommand = new SqlCommand()) {
                    insertCommand.Connection = conn;
                    insertCommand.CommandText = queryString;
                    insertCommand.Parameters.Add("@title", SqlDbType.NVarChar, 50).Value = title;
                    insertCommand.Parameters.Add("@content", SqlDbType.NVarChar, int.MaxValue).Value = content;
                    insertCommand.Parameters.Add("@articleId", SqlDbType.NVarChar, 500).Value = HomeController.PickedArticle.Id;
                    insertCommand.Parameters.Add("@imagePath", SqlDbType.NVarChar, 150).Value = imagePath;

                    try
                    {
                        conn.Open();
                        insertCommand.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        string exception = ex.ToString();
                    }
                    RefreshArticles();
                    HomeController.PickedArticle = GetArticle(HomeController.PickedArticle.Id);
                }
            }
        }
        public static List<DatabaseModels.Reference> RefreshReferences() {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString)) {
                using (SqlCommand selectCommand = new SqlCommand()) {
                    selectCommand.Connection = conn;
                    selectCommand.CommandText = "Select * from [References]";

                    try {
                        conn.Open();
                        selectCommand.ExecuteNonQuery();
                    }
                    catch (SqlException) {

                    }
                    SqlDataReader reader = selectCommand.ExecuteReader();

                    var references = new List<DatabaseModels.Reference>();
                    while (reader.Read()) {
                        references.Add(new DatabaseModels.Reference() {
                            Content = reader["ReferenceContent"].ToString(),
                            Id = Convert.ToInt32(reader["ReferenceId"]),
                            Title = reader["ReferenceTitle"].ToString(),
                            Link = reader["ReferenceLink"].ToString(),
                            ImagePath = reader["ReferenceImagePath"].ToString()
                        });
                    }
                    references.Reverse();
                    return references;
                }
            }
        }
        public static DatabaseModels.Reference GetReference(int ReferenceId) {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConString"].ConnectionString)) {
                using (SqlCommand selectCommand = new SqlCommand()) {
                    selectCommand.Connection = conn;
                    selectCommand.CommandText = "Select * from [References] where ReferenceId = @Id";
                    selectCommand.Parameters.Add("@Id", SqlDbType.Int, 50).Value = ReferenceId;
                    try {
                        conn.Open();
                        selectCommand.ExecuteNonQuery();
                    }
                    catch (SqlException) {
                        return null;
                    }
                    SqlDataReader reader = selectCommand.ExecuteReader();

                    var reference = new DatabaseModels.Reference();
                    while (reader.Read()) {
                        reference.Link = reader["ReferenceLink"].ToString();
                        reference.Content = reader["ReferenceContent"].ToString();
                        reference.Id = Convert.ToInt32(reader["ReferenceId"]);
                        reference.Title = reader["ReferenceTitle"].ToString();
                        reference.ImagePath = reader["referenceImagePath"].ToString();
                    }
                    HomeController.PickedReference = reference;
                }
            }
            return HomeController.PickedReference;
        }
    }
}